import { Component } from "react";

class FooterComponent extends Component{
    render(){
        return (
            <>
                <footer className="bg-dark text-white">
  {/* <!-- Grid container --> */}
  <div className="container p-4">
    {/* <!-- Section: Social media --> */}
   
    {/* <!-- Section: Social media --> */}

    {/* <!-- Section: Form --> */}
  
    {/* <!-- Section: Form --> */}

    {/* <!-- Section: Text --> */}
    <section className="mb-4">
      
    </section>
    {/* <!-- Section: Text --> */}

    {/* <!-- Section: Links --> */}
    <section className="">
      {/* <!--Grid row--> */}
      <div className="row form-group">
        {/* <!--Grid column--> */}
        <div className="col-lg-4 col-md-6 mb-4 mb-md-0">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt distinctio earum
        repellat quaerat voluptatibus placeat nam, commodi optio pariatur est quia magnam
        eum harum corrupti dicta, aliquam sequi voluptate quas.</p>

          <p> © 2018. All right reserved</p>

          
        </div>
        {/* <!--Grid column--> */}

        {/* <!--Grid column--> */}
        <div className="col-lg-4 col-md-6 mb-4 mb-md-0 mt-6">
          <h5 className="text-uppercase">Contacts</h5>
          <h6>Address:</h6>
          <p>lokaka,West Begal, India</p><br/>

          <h6>Email:</h6>
          <p>info@exaple.com</p><br/>
            
            <h6>phones:</h6>
            <p>+91 9999999 or +91 11111111</p>

          
        </div>
        {/* <!--Grid column--> */}

        {/* <!--Grid column--> */}
        <div className="col-lg-4 col-md-6 mb-4 mb-md-0">
          <h5 className="text-uppercase">Links</h5>
            <h6>About</h6>
            <h6>Projects</h6>
            <h6>Blog</h6>
            <h6>Contact</h6>
            <h6>Pricing</h6>
        </div>
        
      </div>
      {/* <!--Grid row--> */}
    </section>
    {/* <!-- Section: Links --> */}
  </div>
  {/* <!-- Grid container -->

  <!-- Copyright --> */}
  <div className="row form-group text-center p-3" >
    <div className="col-3 text-center">Facebook</div>
    <div className="col-3 text-center">instagram</div>
    <div className="col-3 text-center">Twitter</div>
    <div className="col-3 text-center">Google</div>
    
  </div>
  {/* <!-- Copyright --> */}
</footer>
            </>
        )
    }
}

export default FooterComponent;