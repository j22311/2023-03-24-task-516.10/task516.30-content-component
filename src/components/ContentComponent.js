import { Component } from "react";
import listData from "./DataStore";

class ContentComponent extends Component{
    outputListdata(){
       console.log(listData.Products[0].Title);
    }
    render(){
        return (
            <div className="container">
                <div className="row form-group">
                    {/* thêm 1 card   */}
                    {/* <div className="col-4">
                        <div className="card">
                            <div className="card-header text-center">
                               <h5 className="card-title">Banana</h5>
                            </div>
                            <div className="card-body">
                                <img className="card-img-top" style={{maxHeight:"13rem",maxWidth:"12rem"}}  src="http://www.loop54.com/hubfs/demo_images/banana.jpg"></img>
                                <p className="card-text">The banana is an edible fruit, botanically a berry, produced by several kinds of large herbaceous</p>                             <p className="card-text">"Category": "Fruit"</p>
                                <p className="card-text">"Made by": "The banana company",</p>
                                <p className="card-text">"Organic": true,</p>
                                <p className="card-text">"Price": 7</p>
                            </div>
                            <div className="card-footer text-center">
                                <button className="btn btn-success">Add to Cart</button>
                            </div>
                        </div>
                    </div> */}
                    {/* hết một card  */}

                    

                   
                    
                   

                     {/* thêm 1 card   */}
                     {/* <div className="col-4 mt-5">
                        <div className="card">
                            <div className="card-header text-center">
                               <h5 className="card-title">{listData.Products[0].Title}</h5>
                            </div>
                            <div className="card-body">
                                <img className="card-img-top" style={{maxHeight:"13rem",maxWidth:"12rem"}}  src={listData.Products[0].ImageUrl}></img>
                                <p className="card-text">{listData.Products[0].Description.slice(0,80)}</p>                            
                                 <p className="card-text">"Category": {listData.Products[0].Category}</p>
                                <p className="card-text">"Made by": {listData.Products[0].Manufacturer}</p>
                                <p className="card-text">"Organic": {listData.Products[0].Organic.toString()}</p>
                                <p className="card-text">"Price": {listData.Products[0].Price} $</p>
                            </div>
                            <div className="card-footer text-center">
                                <button className="btn btn-success">Add to Cart</button>
                            </div>
                        </div>
                    </div> */}
                    {/* hết một card  */}

                    {/* thêm 1 card   */}
                    
                   {listData.Products.map((value,index) => {
                        if(index < 9){
                            return  <div className="col-4 mt-2">
                            <div className="card shadow p-3 mb-5 bg-white rounded">
                                <div className="card-header text-center">
                                    
                                    <h5 className="card-title">{value.Title}</h5>
                                    
                                   
                                </div>
                                <div className="card-body">
                                    <div className="col-12 text-center">
                                        <img className="card-img-top mt-2" style={{maxHeight:"13rem",maxWidth:"12rem"}}  src={value.ImageUrl}></img>
                                    </div>
                                   
                                    <p className="card-text">{value.Description.slice(0,150)}</p>                            
                                     <p className="card-text">"Category": {value.Category}</p>
                                    <p className="card-text">"Made by": {value.Manufacturer}</p>
                                    <p className="card-text">"Organic": {value.Organic.toString()}</p>
                                    <p className="card-text">"Price": {value.Price} $</p>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn btn-success">Add to Cart</button>
                                </div>
                            </div>
                            </div>
                        }
                       
                   })}
                    {/* hết một card  */}
                    
                </div>
                
            </div>
        )
    }
}

export default ContentComponent;