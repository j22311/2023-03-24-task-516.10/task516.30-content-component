import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import ContentComponent from './components/ContentComponent';
import dataList from './components/DataStore';
function App() {
  return (
    <div className="container-fluid">
        <HeaderComponent/>
        <ContentComponent/>
        <FooterComponent/>
        
    </div>
  );
}

export default App;
